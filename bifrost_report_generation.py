import jaydebeapi
import pandas as pd
from datetime import date,timedelta
import json
import config as config
import os
#from time_series import time_series_main



def get_themes():
    #theme_list = ['slit_hem', 'cowl_neck', 'raw_hem_denim_shorts', 'hem_flare_leg_jeans', 'wrap_coat', 'frill_trim_puff_sleeve', 'pleated_dress', 'layered_skirt', 'sweetheart_neck_top', 'ruffle_blouse', 'ruffle_hem_dress']
    with open(config.EXPANDED_SET_PATH,'r') as f:
            themes = json.load(f)["Expanded_list"]
    new_theme_list = []        
    for theme in themes:
        new_theme_list.append("_".join(theme.split(" ")))
    #print(new_theme_list)    
    return new_theme_list

def fetch_data():
    today = date.today() - timedelta(3)
    splitted_date = str(today).split("-")
    present_date = date(int(splitted_date[0]), int(splitted_date[1]), int(splitted_date[2]))
    days =  timedelta(180)
    previous_date = present_date - days   
    print(present_date)
    print(str(previous_date))


    
    theme_list = get_themes()
    old_df = pd.DataFrame()
    for i,theme in enumerate(theme_list):
        query = """SELECT date(date_parse(y || m || d, '%Y%m%d')) AS Date, count(description) as """+ theme + """ from vorta.mi_product_metrics WHERE date(date_parse(y || m || d, '%Y%m%d')) BETWEEN date """ + "'"  + str("2021-05-21") +"'"+  " and date " + "'"  + str("2021-05-21") +"'" + " and (LOWER(description) LIKE '%"+theme+ "%' or LOWER(title) LIKE '%"+theme+ "%') and style_age = 0 GROUP BY date(date_parse(y || m || d, '%Y%m%d'))"


        with jaydebeapi.connect("com.myntra.bifrost.jdbc.BifrostDriver",
                              "jdbc:bifrost://10.162.140.24:6080/hive",
                              ["nidhi.kumari1", "4kb1aX9rkGKO"],
                                  "/rapid_data/nidhi/bifrost-jdbc-2.0-20210819.133858-1.jar") as conn:
             with conn.cursor() as curs:
                curs.execute(query)
                data = curs.fetchall()
                #curs.fetchmany()

                df = pd.DataFrame(data,columns=tuple([desc[0] for desc in curs.description]))
                df['Date'] = pd.to_datetime(df['Date'])
                df = df.sort_values(by=['Date'])
                df.set_index('Date', inplace=True)
                if i == 0:
                    old_df = df
                if i>=1:
                    old_df = old_df.join(df, how='outer')
                    
    old_df["Date"] = old_df.index
    old_df.reset_index(drop=True, inplace=True)
    old_df["Date"] = old_df["Date"].astype(str)
    
    
    sdate = previous_date   # start date
    edate = present_date
    date_list = pd.date_range(sdate,edate-timedelta(days=1),freq='d')
    date_list = [str(d).split(" ")[0] for d in date_list]  

    empty_date_list = []

    for d in date_list:
        if d not in list(old_df["Date"]):
            empty_date_list.append(d)

    new_df = pd.DataFrame({"Date" : empty_date_list})
    frame = [old_df, new_df]
    df = pd.concat(frame)
    df = df.fillna(0)
    df['Date'] = pd.to_datetime(df['Date'], errors='coerce') 
    df = df.sort_values(by=['Date'])

    column_list = list(df.columns)
    column_names = [i for i in column_list if i != 'Date']
    column_order = ["Date"]+ column_names

    df = df[column_order]
    print(df)
    df.to_csv(config.TIME_SERIES_DATA_PATH, header=True, index = False)
    #return old_df
    return df


#theme_list = get_themes()
#old_df = fetch_data(theme_list)



#data = pd.read_csv(config.TIME_SERIES_DATA_PATH)
#time_series_main(data)
fetch_data()