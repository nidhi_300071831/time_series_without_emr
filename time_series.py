from statsmodels.tsa.statespace.sarimax import SARIMAX
import statsmodels.api as sm
import itertools
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from math import sqrt
from datetime import datetime
import matplotlib.pyplot as plt
from multiprocessing import cpu_count
from joblib import Parallel
from joblib import delayed
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.seasonal import STL
import config
from multiprocessing import Process, Lock, Manager, Pool
from multiprocessing.sharedctypes import Array
import os
import sys
import logging
logging.basicConfig(level=logging.INFO, filename=config.LOGGER_PATH, filemode='w')
#from email_utility import sent_email
#from write_to_excel import Write
from write_in_csv import Write
#from bifrost_report_generation import fetch_data

class Time_series_analysis:

    '''def __init__(self):
        self.data = pd.read_csv(config.TIME_SERIES_DATA_PATH)
        self.theme_list = list(self.data.columns)[1:][:3]
        self.n = len(theme_list)'''


    def root_mean_square_error(self, validCount, predCount):
        return sqrt(mean_squared_error(validCount, predCount))


    def train_test_split(self, df_count):

        train_frac = 0.8
        df_len = len(df_count)
        train_df = df_count[:int(train_frac*df_len)]
        test_df = df_count[int(train_frac*df_len):]
        return train_df, test_df


    def cross_validation(self, df):
        result = []
        p = d = q = range(0, 2)
        pdq = list(itertools.product(p, d, q))
        seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]

        for param in pdq:
            for param_seasonal in seasonal_pdq:
                try:
                    mod_s = sm.tsa.statespace.SARIMAX(df,
                                                    order=param,
                                                    seasonal_order=param_seasonal)

                    results = mod_s.fit()
                    #logging.info('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))
                    result.append((param, param_seasonal, results.aic))
                except:
                    continue
        return result


    def fit_model(self, df, order, seasonal_order):

        mod = sm.tsa.statespace.SARIMAX(df,
                                        order=order,
                                        seasonal_order=seasonal_order)

        results = mod.fit()
        return results


    def predict_model(self, model, test):
        pred_dynamic = model.get_prediction(start=str(test.first_valid_index()).split(' ')[0], end=str(test.last_valid_index()).split(' ')[0], dynamic=True)
        y_forecasted = pred_dynamic.predicted_mean
        pred_dynamic_ci = pred_dynamic.conf_int()


        return y_forecasted, pred_dynamic_ci


    def forecast(self, model, df):
        pred_uc = model.get_forecast(steps=30)
        forecasted = pred_uc.predicted_mean
        pred_ci = pred_uc.conf_int()
        #ax = df.plot(label='observed', figsize=(6, 5))
        #pred_uc.predicted_mean.plot(ax=ax, label='Forecast')
        #ax.fill_between(pred_ci.index,
        #                pred_ci.iloc[:, 0],
        #                pred_ci.iloc[:, 1], color='k', alpha=.25)
        #ax.set_xlabel('Date')
        #ax.set_ylabel('count')
        #plt.legend()
        #plt.show()
        return forecasted


    def stl(self, forcasted):
        try:
            stl = STL(forcasted)
            res = stl.fit()
            res = res.trend.to_frame()
            slope_intercept = np.polyfit([i for i in range(res.shape[0])],res['trend'],1)
            logging.info(slope_intercept)
            return slope_intercept
        except:
            return []


    def main(self, df_count):
        train_df, test_df = self.train_test_split(df_count)
        result = self.cross_validation(train_df)
        result.sort(key = lambda x: x[2])
        model = self.fit_model(train_df, result[0][0], result[0][1])
        #logging.info(model.summary().tables[1])
        y_forecasted, pred_dynamic_ci = self.predict_model(model, test_df)
        y_truth = test_df
        rmse = self.root_mean_square_error(y_truth,y_forecasted)
        #logging.info("rmse",rmse)
        forecasted = self.forecast(model,train_df)
        slope_intercept = self.stl(forecasted)
        return slope_intercept



    def theme_dataframe(self,theme, theme_score_array):
        count = []
        for index, row in data.iterrows():
            count.append((row['Date'],row[theme]))

        df_count = pd.DataFrame(count,columns = ['Day','count'])
        df_count['Day'] = pd.to_datetime(df_count['Day'], errors='coerce')

        df_count['Day'] = df_count['Day'].astype('datetime64[ns]')
        df_count = df_count.resample('W', label='right', closed = 'right', on='Day').sum().reset_index().sort_values(by='Day')
        df_count = df_count.fillna(method='ffill')
        df_count = df_count.set_index('Day')
        df_count.index = pd.to_datetime(df_count.index)
        logging.info(theme)
        logging.info(df_count)
        slope_intercept = self.main(df_count)
        theme_score_array.append((theme,slope_intercept[0]))
        print("theme_df_done")
        
    '''def theme_dataframe(self,theme,data):
        count = []
        for index, row in data.iterrows():
            count.append((row['Date'],row[theme]))

        df_count = pd.DataFrame(count,columns = ['Day','count'])
        df_count['Day'] = pd.to_datetime(df_count['Day'], errors='coerce')

        df_count['Day'] = df_count['Day'].astype('datetime64[ns]')
        df_count = df_count.resample('W', label='right', closed = 'right', on='Day').sum().reset_index().sort_values(by='Day')
        df_count = df_count.fillna(method='ffill')
        df_count = df_count.set_index('Day')
        df_count.index = pd.to_datetime(df_count.index)
        logging.info(theme)
        logging.info(df_count)
        slope_intercept = self.main(df_count)
        #theme_score_array.append((theme,slope_intercept[0]))
        return theme,slope_intercept[0]'''


    

#data = pd.read_csv(config.TIME_SERIES_DATA_PATH)
#theme_list = list(data.columns)[1:][:5]
#n = len(theme_list)
data = pd.read_csv(config.TIME_SERIES_DATA_PATH) #fetch_data()
print(data.head())
theme_list = list(data.columns)[1:]
n = len(theme_list)



if __name__ == "__main__":

    
    
    ts = Time_series_analysis()
    
    #data = df
    #theme_list = list(data.columns)[1:]
    #n = len(theme_list)

    
    #data = pd.read_csv(config.TIME_SERIES_DATA_PATH)
    #theme_list = list(data.columns)[1:][:3]
    #n = len(theme_list)
    #logging.info("theme_listt----",theme_list)
    theme_score = []
    print("starting")
    with Manager() as manager:
        L = manager.list()  # <-- can be shared between processes.
        pool = Pool()
        for i in range(n):
            #logging.info("L-------",L)
            #logging.info("theme_list[i]------",theme_list[i])
            pool.apply_async(ts.theme_dataframe, args=(theme_list[i], L))  # Passing the list
        pool.close()
        pool.join()
            #processes.append(p)
        print("finish")
        logging.info (L)
        theme_score = list(L)
        print(theme_score)


        theme_score.sort(key=lambda x: x[1])
        ranked_list_of_themes = []
        for index,t in enumerate(theme_score[::-1]):
            ranked_list_of_themes.append((t[0],index,t[1]))
            

        #df = pd.DataFrame(ranked_list_of_themes, columns=['Theme_list', 'rank', 'score'])
        #df.to_csv(config.RANKED_THEME_OUTPUT_PATH,header=True,index=False)
        minimum = min(len(ranked_list_of_themes),80)
        theme_list = [t[0] for t in ranked_list_of_themes][:minimum]
        print(theme_list)
        w = Write(theme_list)
        w.main()
        #sent_email()


'''
def time_series_main(data):
    #data = pd.read_csv("sample_time_series_input.csv")
    theme_list = list(data.columns)[1:]
    n = len(theme_list)
    ts = Time_series_analysis()
    theme_score = []
    for i in range(n):
        theme,slope_intercept = ts.theme_dataframe(theme_list[i], data)
        theme_score.append((theme,slope_intercept ))
    theme_score.sort(key=lambda x: x[1])
    ranked_list_of_themes = []
    for index,t in enumerate(theme_score[::-1]):
        ranked_list_of_themes.append((t[0],index,t[1]))
    minimum = min(len(ranked_list_of_themes),80)
    theme_list = [t[0] for t in ranked_list_of_themes][:minimum]
    #print(theme_list)
    w = Write(theme_list)
    w.main()
    return theme_list
'''
#data = pd.read_csv(config.TIME_SERIES_DATA_PATH)
#time_series_main()