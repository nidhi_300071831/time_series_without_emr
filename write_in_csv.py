import pandas as pd
import config as config
import os
import ast
import json
import jaydebeapi
import pandas as pd
from datetime import date,timedelta
import json
import config as config
import os

today = date.today() - timedelta(6)
splitted_date = str(today).split("-")
present_date = date(int(splitted_date[0]), int(splitted_date[1]), int(splitted_date[2]))


class Write:


    def __init__(self,theme_list):
        
        self.theme_list = theme_list
        
        
    def query_for_image(self):
        theme_lists = []
        image_list = []
        for theme in self.theme_list[:2]:
            query = """SELECT date(date_parse(y || m || d, '%Y%m%d')) AS Date, product_id, primary_image_url
            from vorta.mi_product_metrics \
            WHERE date(date_parse(y || m || d, '%Y%m%d')) BETWEEN date """ + "'"  + str(present_date) +"'"+  " and date" + " '"  + str(present_date) +"'" + " and LOWER(description) LIKE '%"+theme+ "%'"
            #print(query)
            with jaydebeapi.connect("com.myntra.bifrost.jdbc.BifrostDriver",
                              "jdbc:bifrost://10.162.140.24:6080/hive",
                              ["nidhi.kumari1", "4kb1aX9rkGKO"],
                                  "/rapid_data/nidhi/bifrost-jdbc-2.0-20210819.133858-1.jar") as conn:
                 with conn.cursor() as curs:
                    curs.execute(query)
                    data = curs.fetchall()
                    df = pd.DataFrame(data,columns=tuple([desc[0] for desc in curs.description]))
                    #print(theme, df.shape)
                    if len(df)>20:
                        theme_lists.append(theme)
                        img_list = []
                        for index,row in df.iterrows():
                            try:
                                img_list.append(ast.literal_eval(row['primary_image_url'])[0])
                            except:
                                img_list.append(row['primary_image_url'])
                        image_list.append(img_list[:10])
        #print(theme_lists, image_list)
        return theme_lists, image_list
  
    
    def write_to_csv(self, theme_lists, image_list):
        empty_df = pd.DataFrame(columns = ['Remark (OK/ Reject)', 'Must have/ OK - Recommended', 'Already existing/ New','Type','Rename'])
        image_df = pd.DataFrame(image_list, columns = ['image' + str(i+1) for i in range(10)])
        themes_df = pd.DataFrame({'Themes':theme_lists})
        frames = [empty_df, themes_df, image_df]

        result = pd.concat(frames, axis = 1)
        result.to_csv(config.RANKED_THEME_OUTPUT_PATH, index=False, header=True)
        print("write is successful")
        
    def write_to_json(self,theme_lists):
        
        with open(config.ALREADY_EXISTING_THEMES,'r') as f:
            themes = json.load(f)["already_existing_list"]
        
        
        json_obj = {}
        json_obj["already_existing_list"] = list(set(theme_lists + themes))
        with open(config.ALREADY_EXISTING_THEMES,"w") as f:
            json.dump(json_obj,f)
        print("already existing list updated")
    
    
    def main(self):
        theme_lists, image_list = self.query_for_image()
        self.write_to_csv(theme_lists,image_list)
        #self.write_to_json(theme_lists)